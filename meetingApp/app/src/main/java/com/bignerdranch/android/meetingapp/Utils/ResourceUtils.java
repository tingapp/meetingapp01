package com.bignerdranch.android.meetingapp.Utils;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bignerdranch.android.meetingapp.Model.ImageDrawable;
import com.bignerdranch.android.meetingapp.application.GlobalApplication;
import com.bignerdranch.android.meetingapp.connection.DrawableResponseHandler;
import com.bignerdranch.android.meetingapp.threading.ImageFileLoadWork;
import com.bignerdranch.android.meetingapp.threading.ThreadManager;
import com.bignerdranch.android.meetingapp.threading.Work;

public class ResourceUtils {
    public static String getString(int resId) {
        return GlobalApplication.getInstance().getResources().getString(resId);
    }

    public static Drawable getDrawable(int resId) {
        return GlobalApplication.getInstance().getResources().getDrawable(resId);
    }

    public static void getImageFile(ImageView imageView, String url, int width, int height) {
        if (url == null || url.isEmpty())
            return;

        Work<ImageDrawable> work = new ImageFileLoadWork(url, width, height);
        ThreadManager.execute(work, new DrawableResponseHandler(imageView, true));
    }
}
