package com.bignerdranch.android.meetingapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bignerdranch.android.meetingapp.Fragment.MainFragment;
import com.bignerdranch.android.meetingapp.Fragment.MatchedTeamFragment;
import com.bignerdranch.android.meetingapp.Fragment.ProfileFragment;
import com.bignerdranch.android.meetingapp.Fragment.UnmatchedTeamFragment;
import com.bignerdranch.android.meetingapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.UUID;

/**
 * Created by Chaosphch on 2016-07-17.
 */
public class MainActivity extends FragmentActivity {

    private int mPrevPosition;
    private ViewPager MainViewPager;

    private LinearLayout mMainButton;
    private LinearLayout mMatchedTeamButton;
    private LinearLayout mUnmatchedTeamButton;
    private LinearLayout mProfilebutton;

    private Fragment[] mMainViewPagerList;
    private ImageView mMainImage;
    private ImageView mMatchedTeamImage;
    private ImageView mUnmatchedTeamImage;
    private ImageView mProfileImage;
    private ImageView[] mToolbarImageList;
    private int[] toolbarUncheckedPictureList = new int[] {
            R.drawable.ic_menu_home,
            R.drawable.ic_menu_chat,
            R.drawable.ic_menu_team,
            R.drawable.ic_menu_profile};
    private int[] toolbarCheckedPictureList = new int[] {
            R.drawable.ic_menu_home_checked,
            R.drawable.ic_menu_chat_checked,
            R.drawable.ic_menu_team_checked,
            R.drawable.ic_menu_profile_checked};

//    private LinearLayout mToolbar;
//    private LinearLayout mDetailViewPageMark;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainViewPagerList = new Fragment[] {
                new MainFragment(),
                new MatchedTeamFragment(),
                new UnmatchedTeamFragment(),
                new ProfileFragment()
        };

        FragmentManager fm = getSupportFragmentManager();

        MainViewPager = (ViewPager) findViewById(R.id.activity_main_container);
//        mToolbar = (LinearLayout)findViewById(R.id.activity_main_toolbar);
//        mDetailViewPageMark = (LinearLayout)findViewById(R.id.activity_main_page_mark);

        mMainImage = (ImageView)findViewById(R.id.activity_main_main_image);
        mMatchedTeamImage = (ImageView)findViewById(R.id.activity_main_matchedteam_image);
        mUnmatchedTeamImage = (ImageView)findViewById(R.id.activity_main_unmatchedteam_image);
        mProfileImage = (ImageView)findViewById(R.id.activity_main_profile_image);
        mToolbarImageList = new ImageView[] {mMainImage, mMatchedTeamImage, mUnmatchedTeamImage, mProfileImage};

        MainViewPager.setAdapter(new FragmentPagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                return mMainViewPagerList[position];
            }

            @Override
            public int getCount() {
                return mMainViewPagerList.length;
            }
        });
        mPrevPosition = 0;
        MainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //이전 페이지에 해당하는 페이지 표시 이미지 변경
//                mDetailViewPageMark.getChildAt(mPrevPosition).setBackgroundResource(R.drawable.ic_icon_dot_unchecked);
                mToolbarImageList[mPrevPosition].setImageResource(toolbarUncheckedPictureList[mPrevPosition]);

                //현재 페이지에 해당하는 페이지 표시 이미지 변경
//                mDetailViewPageMark.getChildAt(position).setBackgroundResource(R.drawable.ic_icon_dot_checked);
                mToolbarImageList[position].setImageResource(toolbarCheckedPictureList[position]);
                mPrevPosition = position;                //이전 포지션 값을 현재로 변경
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        initPageMark();

        mMainButton = (LinearLayout) findViewById(R.id.activity_main_main_button);
        mMatchedTeamButton = (LinearLayout) findViewById(R.id.activity_main_matchedteam_button);
        mUnmatchedTeamButton = (LinearLayout) findViewById(R.id.activity_main_unmatchedteam_button);
        mProfilebutton = (LinearLayout)findViewById(R.id.activity_main_profile_button);

        mMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainViewPager.setCurrentItem(0,true);
            }
        });
        mMatchedTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainViewPager.setCurrentItem(1,true);
            }
        });
        mUnmatchedTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainViewPager.setCurrentItem(2,true);
            }
        });
        mProfilebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainViewPager.setCurrentItem(3,true);
            }
        });
    }


/*    private void initPageMark(){

        for(int i=0; i<4; i++)
        {
            ImageView iv = new ImageView(getApplicationContext());
            iv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            if(i==0)
                iv.setBackgroundResource(R.drawable.ic_icon_dot_checked);
            else
                iv.setBackgroundResource(R.drawable.ic_icon_dot_unchecked);

            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mMainViewPager.setCurrentItem(1, true);
                }
            });

            mToolbar.addView(iv);
        }
    }*/
}