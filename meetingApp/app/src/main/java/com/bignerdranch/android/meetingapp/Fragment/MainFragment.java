package com.bignerdranch.android.meetingapp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.android.meetingapp.Activity.MainDetailActivity;
import com.bignerdranch.android.meetingapp.R;

/**
 * Created by Chaosphch on 2016-07-15.
 */
public class MainFragment extends Fragment {

    private View button1;
//    private View btnHome;
//    ToggleButton togglebutton = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        button1 = v.findViewById(R.id.btn_teamprofile_01);
//        btnHome = v.findViewById(R.id.clickHome_main);


//        togglebutton = (ToggleButton) v.findViewById(R.id.like_it);
//        togglebutton.setOnCheckedChangeListener(this);
//        togglebutton.setChecked(false);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MainFragment","onClick");
                Intent intent = new Intent(getActivity(), MainDetailActivity.class);
                startActivity(intent);
            }
        });
        Log.d("MainFragment", "createview");

//        btnHome.setOnClickListener(new View.OnClickListener() {
//
//        });



//        @Override
//        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            Toast.makeText(this,
//                    isChecked ? togglebutton.getTextOn() : togglebutton.getTextOff(),
//                    Toast.LENGTH_SHORT).show();
//        }

        return v;
    }
}