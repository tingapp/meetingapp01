package com.bignerdranch.android.meetingapp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.android.meetingapp.Activity.UnmatchedTeamDetailActivity;
import com.bignerdranch.android.meetingapp.Activity.UnmatchedTeamOrganizeTeamActivity;
import com.bignerdranch.android.meetingapp.Model.UnmatchedTeam;
import com.bignerdranch.android.meetingapp.R;

/**
 * Created by JiwoonWon on 16. 7. 28..
 */
public class UnmatchedTeamFragment extends Fragment {

    private RecyclerView mUnmatchedRecyclerView;
    private UnmatchedTeamAdapter mAdapter;
    private View organizeTeam;
    private UnmatchedTeam[] temporaryUnmatchedTeamList = new UnmatchedTeam[] {
            new UnmatchedTeam(3, "원지운, 정현우, 이재승, 손상혁", "4/5", "선릉"),
            new UnmatchedTeam(3, "원운, 정현, 히히하, 손혁", "6/3", "강남"),
            new UnmatchedTeam(3, "아이유, 정현우, 이재, 손상혁", "3/3", "화성"),
            new UnmatchedTeam(3, "원지운, 조혜민, 이재승, 손혁", "4/5", "과천")
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_unmatchedteam, container, false);
        Log.d("UnmatchedTeamFragment", "just after inflating");

        organizeTeam  = v.findViewById(R.id.fragment_unmatchedteam_organize_team);
        organizeTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UnmatchedTeamOrganizeTeamActivity.class);
                startActivity(intent);
            }
        });
        Log.d("UnmatchedTeamFragment", "before find recyclerview");

        mUnmatchedRecyclerView = (RecyclerView) v.findViewById(R.id.fragment_unmatchedteam_recycler_view);
        mUnmatchedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();
        return v;
    }

    private void updateUI() {
        UnmatchedTeam[] UnmatchedTeams = temporaryUnmatchedTeamList;
        Log.d("UnmatchedteamFragment","before setting recyclerview adapter");
        mAdapter = new UnmatchedTeamAdapter(UnmatchedTeams);
        mUnmatchedRecyclerView.setAdapter(mAdapter);
        Log.d("UnmatchedTeamFragment","updateUI");
    }

    private class UnmatchedTeamHolder extends RecyclerView.ViewHolder {
        private UnmatchedTeam mUnmatchedTeam;
        private LinearLayout mWhole;
        private TextView mUserNumber;
        private TextView mCalenderText;

        public UnmatchedTeamHolder(View itemView) {
            super(itemView);

            mWhole = (LinearLayout) itemView.findViewById(R.id.item_unmatchedteam_whole);
            mUserNumber = (TextView) itemView.findViewById(R.id.item_unmatchedteam_user_number);
            mCalenderText = (TextView) itemView.findViewById(R.id.item_unmatchedteam_calander_text);
        }

        public void bindUnmatchedTeam(UnmatchedTeam unmatchedTeam) {
            mUnmatchedTeam = unmatchedTeam;
            mUserNumber.setText(Integer.toString(mUnmatchedTeam.getUserNumber()));
            mCalenderText.setText(mUnmatchedTeam.getUnmatchedUserDates());

            mWhole.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("UnmatchedTekamFragment", "onClickItem");
                    Intent intent = new Intent(getActivity(), UnmatchedTeamDetailActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    private class UnmatchedTeamAdapter extends RecyclerView.Adapter<UnmatchedTeamHolder> {
        private UnmatchedTeam[] mUnmatchedTeams;

        public UnmatchedTeamAdapter(UnmatchedTeam[] unmatchedTeams) {
            mUnmatchedTeams = unmatchedTeams;
        }

        @Override
        public UnmatchedTeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.item_unmatchedteam, parent, false);
            Log.d("UnmatchedTeamFragment", "onCreateViewholder");
            return new UnmatchedTeamHolder(view);
        }

        @Override
        public void onBindViewHolder(UnmatchedTeamHolder holder, int position) {
            UnmatchedTeam unmatchedTeam = mUnmatchedTeams[position];
            holder.bindUnmatchedTeam(unmatchedTeam);
            Log.d("UnmatchedTeamFragment","onBindViewHolder");
        }

        @Override
        public int getItemCount() {
            return mUnmatchedTeams.length;
        }
    }
}
