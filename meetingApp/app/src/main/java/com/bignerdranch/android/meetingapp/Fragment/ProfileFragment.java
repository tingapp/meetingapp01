package com.bignerdranch.android.meetingapp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bignerdranch.android.meetingapp.Activity.ProfileEditActivity;
import com.bignerdranch.android.meetingapp.Activity.ProfileSettingsActivity;
import com.bignerdranch.android.meetingapp.R;

/**
 * Created by Chaosphch on 2016-07-30.
 */
public class ProfileFragment extends Fragment {

    private View mEditProfile;
    private Button mEditSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        mEditProfile = v.findViewById(R.id.fragment_profile_edit_profile);
        mEditSettings = (Button) v.findViewById(R.id.fragment_profile_edit_settings);

        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
                startActivity(intent);
            }
        });
        mEditSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileSettingsActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

}
