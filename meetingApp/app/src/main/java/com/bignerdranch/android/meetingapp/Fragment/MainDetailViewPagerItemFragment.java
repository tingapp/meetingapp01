package com.bignerdranch.android.meetingapp.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.android.meetingapp.R;

/**
 * Created by Chaosphch on 2016-07-28.
 */
public class MainDetailViewPagerItemFragment extends Fragment {
    private static final String STRING_LIST = "string_list";
    private static final String ITEM_POSITION = "item_position";

    public static MainDetailViewPagerItemFragment newInstance(String[] stringList, int position) {
        Bundle args = new Bundle();
        args.putSerializable(STRING_LIST, stringList);
        args.putSerializable(ITEM_POSITION, position);

        MainDetailViewPagerItemFragment fragment = new MainDetailViewPagerItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.item_main_detail_view_pager, container, false);

        TextView viewPagerItemTextView = (TextView)v.findViewById(R.id.item_main_detail_view_pager_text_view);
        String[] stringList = (String[])getArguments().getSerializable(STRING_LIST);
        int position = (int)getArguments().getSerializable(ITEM_POSITION);

        viewPagerItemTextView.setText(stringList[position]);
        return v;
    }
}