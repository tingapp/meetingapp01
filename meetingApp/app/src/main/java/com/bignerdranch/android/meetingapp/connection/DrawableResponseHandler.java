package com.bignerdranch.android.meetingapp.connection;

import android.graphics.Bitmap;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.bignerdranch.android.meetingapp.Model.ImageDrawable;
import com.bignerdranch.android.meetingapp.R;
import com.bignerdranch.android.meetingapp.Utils.BitmapUtils;
import com.bignerdranch.android.meetingapp.application.GlobalApplication;

public class DrawableResponseHandler extends ResponseHandler<ImageDrawable> {
	private ImageView imageView;
	private boolean isCircular;

	public DrawableResponseHandler(ImageView imageView, boolean isCircular) {
		super();
		this.imageView = imageView;
		this.isCircular = isCircular;
	}

	@Override
	protected void onResponse(ImageDrawable res) {
		if (imageView == null || res == null)
			return;

		if (res.isFadeIn()) {
			Animation fadeIn = AnimationUtils.loadAnimation(GlobalApplication.getInstance(), R.anim.fade_in);
			imageView.startAnimation(fadeIn);
		}

		if (isCircular) {
			Bitmap b = res.getDrawable().getBitmap();
			imageView.setImageBitmap(BitmapUtils.cropCircularView(b));
		} else
			imageView.setImageDrawable(res.getDrawable());
	}
}
