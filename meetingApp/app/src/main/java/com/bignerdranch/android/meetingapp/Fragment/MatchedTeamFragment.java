package com.bignerdranch.android.meetingapp.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.android.meetingapp.Model.MatchedTeam;
import com.bignerdranch.android.meetingapp.R;

/**
 * Created by JiwoonWon on 16. 7. 28..
 */
public class MatchedTeamFragment extends Fragment {

    private RecyclerView mMatchedRecyclerView;
    private MatchedTeamAdapter mAdapter;
    private MatchedTeam[] temporaryMatchedTeamList = new MatchedTeam[] {
            new MatchedTeam(6,"04.05","정현우"),
            new MatchedTeam(8,"04.08","원운"),
            new MatchedTeam(6,"04.11","아이유"),
            new MatchedTeam(4,"04.15","조혜민")
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_matchedteam, container, false);
        mMatchedRecyclerView = (RecyclerView) v.findViewById(R.id.fragment_matchedteam_recycler_view);
        mMatchedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();
        return v;
    }

    private void updateUI() {
        MatchedTeam[] matchedTeams = temporaryMatchedTeamList;

        mAdapter = new MatchedTeamAdapter(matchedTeams);
        mMatchedRecyclerView.setAdapter(mAdapter);
    }

    private class MatchedTeamHolder extends RecyclerView.ViewHolder {
        private MatchedTeam mMatchedTeam;
        private TextView mMatchedUserNames;
        private TextView mUserNumber;
        private TextView mChatLastTime;
        private TextView mChatLastText;


        public MatchedTeamHolder(View itemView) {
            super(itemView);
            mMatchedUserNames = (TextView) itemView.findViewById(R.id.item_matchedteam_user_names);
            mUserNumber = (TextView) itemView.findViewById(R.id.item_matchedteam_user_number);
            mChatLastTime = (TextView) itemView.findViewById(R.id.item_matchedteam_chat_last_time);
            mChatLastText = (TextView) itemView.findViewById(R.id.item_matchedteam_chat_last_text);
        }

        public void bindMatchedTeam(MatchedTeam matchedTeam) {
            mMatchedTeam = matchedTeam;
            mMatchedUserNames.setText(mMatchedTeam.getMatchedUserNames());
            mUserNumber.setText(mMatchedTeam.getMatchedUserNames());
            mChatLastText.setText("안녕안녕안녕");
            mChatLastTime.setText("4:54");
        }
    }

    private class MatchedTeamAdapter extends RecyclerView.Adapter<MatchedTeamHolder> {
        private MatchedTeam[] mMatchedTeams;

        public MatchedTeamAdapter(MatchedTeam[] matchedTeams) {
            mMatchedTeams = matchedTeams;
        }

        @Override
        public MatchedTeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.item_matchedteam, parent, false);
            return new MatchedTeamHolder(view);
        }

        @Override
        public void onBindViewHolder(MatchedTeamHolder holder, int position) {
            MatchedTeam matchedTeam = mMatchedTeams[position];
            holder.bindMatchedTeam(matchedTeam);
        }

        @Override
        public int getItemCount() {
            return mMatchedTeams.length;
        }
    }
}