package com.bignerdranch.android.meetingapp.Activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bignerdranch.android.meetingapp.R;
import com.bignerdranch.android.meetingapp.Utils.PictureOptionsDialog;
import com.bignerdranch.android.meetingapp.Utils.ResourceUtils;

import java.util.List;

/**
 * Created by macbook on 16. 8. 6..
 */
public class ProfileEditActivity extends AppCompatActivity implements PictureOptionsDialog.OnPictureOptionClickListener {

    private int REQUEST_GET_IMAGE = 0;
    private String imagePath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GET_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri image = data.getData();
            String mediaData = MediaStore.Images.Media.DATA;
            String[] projection = {mediaData};
            ContentResolver contentResolver = getContentResolver();
            Cursor cursor = contentResolver.query(image, projection, null, null, null);
            cursor.moveToFirst();

            int column = cursor.getColumnIndex(mediaData);
            imagePath = cursor.getString(column);

            cursor.close();

            setImage();
        }
    }

    public void getImage(View view) {
        PictureOptionsDialog dialog = new PictureOptionsDialog();
        dialog.setOnPictureOptionClickListener(this);
        dialog.show(getSupportFragmentManager(), "pictureOptions");
    }

    @Override
    public void onPictureOptionClick(int position) {
        Intent intent;
        if (position == 0) {
            intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        } else {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        PackageManager manager = getPackageManager();
        List<ResolveInfo> activities = manager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;
        if (isIntentSafe)
            startActivityForResult(intent, REQUEST_GET_IMAGE);
    }

    private void setImage() {
        if (imagePath != null && !imagePath.isEmpty()) {
            ImageView imageView = (ImageView) findViewById(R.id.activity_profile_edit_main_image);
            ResourceUtils.getImageFile(imageView, imagePath, 300, 300);
        }
    }
}
