package com.bignerdranch.android.meetingapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.bignerdranch.android.meetingapp.R;

/**
 * Created by macbook on 16. 8. 6..
 */
public class ProfileSettingsActivity extends AppCompatActivity {

    private Button mTeamBlock;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);

        mTeamBlock = (Button) findViewById(R.id.activity_profile_settings_team_block);
        mTeamBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileSettingsActivity.this, ProfileSettingsTeamBlockActivity.class);
                startActivity(intent);
            }
        });
    }
}
