package com.bignerdranch.android.meetingapp.application;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

/**
 * Created by macbook on 16. 8. 9..
 */
public class GlobalApplication extends Application {
    public static final String PREF_APP = "prefApp";
    public static final String PROPERTY_ON_SCREEN = "propertyOnScreen";

    private static GlobalApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Log.d(getClass().getSimpleName(), "onCreate");
    }

    @Override
    public void onTerminate() {
        Log.d(getClass().getSimpleName(), "onTerminate");
        instance = null;
        super.onTerminate();
    }

    public static GlobalApplication getInstance() {
        Log.d("GlobalApplication", "getInstance : " + instance);
        return instance;
    }

    public static int getAppVersion() {
        try {
            PackageInfo packageInfo = instance.getPackageManager().getPackageInfo(instance.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getPref() {
        return getSharedPreferences(PREF_APP, MODE_PRIVATE);
    }

}
