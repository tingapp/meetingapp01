package com.bignerdranch.android.meetingapp.Model;

import java.util.UUID;

/**
 * Created by Chaosphch on 2016-08-02.
 */
public class UnmatchedTeam {
    private UUID mUnmatchedTeamId;
    private Integer mUserNumber;
    private String mUnmatchedUserNames;
    private String mUnmatchedUserDates;
    private String mUnmatchedUserLocations;
    private Integer mMyIndex;

    public void setUnmatchedUserDates(String unmatchedUserDates) {
        mUnmatchedUserDates = unmatchedUserDates;
    }

    public void setUnmatchedUserLocations(String unmatchedUserLocations) {
        mUnmatchedUserLocations = unmatchedUserLocations;
    }

    public UUID getUnmatchedTeamId() {
        return mUnmatchedTeamId;
    }

    public int getUserNumber() {
        return mUserNumber;
    }

    public String getUnmatchedUserNames() {
        return mUnmatchedUserNames;
    }

    public String getUnmatchedUserDates() {
        return mUnmatchedUserDates;
    }

    public String getUnmatchedUserLocations() {
        return mUnmatchedUserLocations;
    }

    public int getMyIndex() {
        return mMyIndex;
    }

    // constructor for develop

    public UnmatchedTeam(int number, String names, String dates, String locations) {
        mUnmatchedTeamId = UUID.randomUUID();
        mUserNumber = number;
        mUnmatchedUserNames = names;
        mUnmatchedUserDates = dates;
        mUnmatchedUserLocations = locations;

    }

    // end constructor for develop
}