package com.bignerdranch.android.meetingapp.threading;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import com.bignerdranch.android.meetingapp.Model.ImageDrawable;
import com.bignerdranch.android.meetingapp.Utils.BitmapUtils;
import com.bignerdranch.android.meetingapp.application.GlobalApplication;

import java.io.IOException;

public class ImageFileLoadWork extends Work<ImageDrawable> {
    private String path;
    private int width;
    private int height;

    public ImageFileLoadWork(String path, int width, int height) {
        super();
        this.path = path;
        this.width = width;
        this.height = height;
    }

    @Override
    public ImageDrawable work() throws IOException {
        Bitmap bitmap = BitmapUtils.decodeFileScaledDown(path, width, height);
        bitmap = BitmapUtils.fixOrientation(path, bitmap);
        Log.d(getClass().getSimpleName(), "before getApplication");
        BitmapDrawable drawable = new BitmapDrawable(GlobalApplication.getInstance().getResources(), bitmap);
        return new ImageDrawable(drawable, true);
    }
}
