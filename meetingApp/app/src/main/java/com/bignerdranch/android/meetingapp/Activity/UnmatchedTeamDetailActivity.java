package com.bignerdranch.android.meetingapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.bignerdranch.android.meetingapp.R;

/**
 * Created by macbook on 16. 8. 4..
 */
public class UnmatchedTeamDetailActivity extends AppCompatActivity {

    private Button mTeamIntroduction;
    private Button mWishLocation;
    private Button mWishDate;
    private Button mChangeTeammate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unmatched_team_detail);

        mTeamIntroduction = (Button) findViewById(R.id.activity_unmatched_team_detail_team_introduction);
        mWishLocation = (Button) findViewById(R.id.activity_unmatched_team_detail_wish_location);
        mWishDate = (Button) findViewById(R.id.activity_unmatched_team_detail_wish_date);
        mChangeTeammate = (Button) findViewById(R.id.activity_unmatched_team_detail_change_teammate);

        mTeamIntroduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        mWishLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UnmatchedTeamDetailActivity.this, UnmatchedTeamDetailWishLocationActivity.class);
                startActivity(intent);
            }
        });
        mWishDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UnmatchedTeamDetailActivity.this, UnmatchedTeamDetailWishDateActivity.class);
                startActivity(intent);
            }
        });
        mChangeTeammate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UnmatchedTeamDetailActivity.this, UnmatchedTeamDetailChangeTeammateActivity.class);
                startActivity(intent);
            }
        });
    }
}
