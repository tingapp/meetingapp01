package com.bignerdranch.android.meetingapp.Model;

import java.util.UUID;

/**
 * Created by Chaosphch on 2016-07-19.
 */
public class Profile {

    private UUID mId;
    private String mUsername;
    private String mPassword;
    private int mAge;

    public Profile() {
        this(UUID.randomUUID());
    }

    public Profile(UUID id) {
        mId = id;
    }

    public UUID getId() {
        return mId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(int age) {
        mAge = age;
    }
}