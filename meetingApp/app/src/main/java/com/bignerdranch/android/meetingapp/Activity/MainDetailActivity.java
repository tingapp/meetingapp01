package com.bignerdranch.android.meetingapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bignerdranch.android.meetingapp.Fragment.MainDetailViewPagerItemFragment;
import com.bignerdranch.android.meetingapp.R;

/**
 * Created by Chaosphch on 2016-07-15.
 */
public class MainDetailActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;
    private int mPrevPosition;
    private ViewPager mDetailViewPager;
    private LinearLayout mDetailViewPageMark;

    private String[] temporaryViewPagerContent1 = new String[] {"원지운1", "원지운2","원지운3","원지운4"};
    private String[] temporaryViewPagerContent2 = new String[] {"정현우1", "정현우2","정현우3"};
    private String[] temporaryViewPagerContent3 = new String[] {"이재승1"};
    private String[] temporaryViewPagerContent4 = new String[] {"손상혁1", "손상혁2","손상혁3","손상혁4"};
    private static String[] mStringArray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_detail);

        mDetailViewPager = (ViewPager) findViewById(R.id.activity_main_detail_view_pager);
        mFragmentManager = getSupportFragmentManager();
        takeProfile1(findViewById(R.id.activity_main_detail_teammate_1));

        mDetailViewPageMark = (LinearLayout)findViewById(R.id.activity_main_detail_page_mark);
        mDetailViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //이전 페이지에 해당하는 페이지 표시 이미지 변경
//                mDetailViewPageMark.getChildAt(mPrevPosition).setBackgroundResource(R.drawable.ic_icon_dot_unchecked);

                //현재 페이지에 해당하는 페이지 표시 이미지 변경
//                mDetailViewPageMark.getChildAt(position).setBackgroundResource(R.drawable.ic_icon_dot_checked);
                mPrevPosition = position;                //이전 포지션 값을 현재로 변경
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        initPageMark();
    }

    public void backToMain(View view) {
        Intent intent = new Intent(MainDetailActivity.this, MainActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);
        Log.i("FlagActivity", "Clear");
        startActivity(intent);
    }

    public void takeProfile1(View view){
        takeProfile(temporaryViewPagerContent1);
    }
    public void takeProfile2(View view) {
        takeProfile(temporaryViewPagerContent2);
    }
    public void takeProfile3(View view) {
        takeProfile(temporaryViewPagerContent3);
    }
    public void takeProfile4(View view) {
        takeProfile(temporaryViewPagerContent4);
    }
    public void takeProfile(String[] stringArray) {
        mStringArray = stringArray;
        mDetailViewPager.setAdapter(new FragmentStatePagerAdapter(mFragmentManager) {
            @Override
            public Fragment getItem(int position) {
                return MainDetailViewPagerItemFragment.newInstance(mStringArray, position);
            }
            @Override
            public int getCount() {
                return mStringArray.length;
            }
        });
    }

/*    private void initPageMark(){
        COUNT = 6;
        for(int i=0; i<COUNT; i++)
        {
            ImageView iv = new ImageView(getApplicationContext());
            iv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            if(i==0)
                iv.setBackgroundResource(R.drawable.ic_icon_dot_checked);
            else
                iv.setBackgroundResource(R.drawable.ic_icon_dot_unchecked);

            mDetailViewPageMark.addView(iv);
        }
        mPrevPosition = 0;
    }*/
}
