package com.bignerdranch.android.meetingapp.Model;

import java.util.UUID;

/**
 * Created by Chaosphch on 2016-08-02.
 */
public class MatchedTeam {

    private UUID mMatchedTeamId;
    private int mMatchedTeamUserNumber;
    private String mMatchedDate;
    private String mMatchedUserNames;

    public UUID getMatchedTeamId() {
        return mMatchedTeamId;
    }

    public int getMatchedTeamUserNumber() {
        return mMatchedTeamUserNumber;
    }

    public String getMatchedDate() {
        return mMatchedDate;
    }

    public String getMatchedUserNames() {
        return mMatchedUserNames;
    }

    //  constructor for develop

    public MatchedTeam(int usernumber, String matcheddate, String usernames) {
        mMatchedTeamId = UUID.randomUUID();
        mMatchedTeamUserNumber = usernumber;
        mMatchedDate = matcheddate;
        mMatchedUserNames = usernames;
    }

    // end constructor for develop
}
